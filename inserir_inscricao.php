<!DOCTYPE html>
<html>
<head>
	<title>Inserir inscrição</title>
	<link rel="stylesheet" type="text/css" href="_css/estilo.css">
	<link rel="stylesheet" type="text/css" href="_css/mensagem.css">
</head>
<body>
	<div id="interface">
		<header id="cabecalho">
			<img src="_imagens/logo.png" id="logo">
			<h1>&lsaquo;Kursando cursos&rsaquo;</h1>
			<h2>&lsaquo; Programando para Web &frasl;&rsaquo;</h2>			
		</header>

		<?php		

		try{
			$conn = new PDO ('mysql:host=localhost;dbname=kc_cursos','root','');
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			$stmt = $conn->prepare('INSERT INTO ficha (nome, cpf, data, endereco, numero, bairro, cidade, estado, telefone, email) VALUES (:nome, :cpf, :data, :endereco, :numero, :bairro, :cidade, :estado, :telefone, :email)');
			$nome = $_POST['nome'];
			$cpf = $_POST['cpf'];
			$data = $_POST['data'];
			$endereco = $_POST['endereco'];
			$numero = $_POST['numero'];
			$bairro = $_POST['bairro'];
			$cidade = $_POST['cidade'];
			$estado = $_POST['estado'];
			$telefone = $_POST['telefone'];
			$email = $_POST['email'];
			$stmt->bindValue(':nome', $nome);
			$stmt->bindValue(':cpf', $cpf);
			$stmt->bindValue(':data', $data);
			$stmt->bindValue(':endereco', $endereco);
			$stmt->bindValue(':numero', $numero);
			$stmt->bindValue(':bairro', $bairro);
			$stmt->bindValue(':cidade', $cidade);
			$stmt->bindValue(':estado', $estado);
			$stmt->bindValue(':telefone', $telefone);
			$stmt->bindValue(':email', $email);
			$stmt->execute();
			echo "Inscrição efetuada com sucesso, em breve entraremos em contato!";
		}
		catch (PDOException $e){
			echo 'ERROR: '.$e->getMessage();
		} 
		?>
		<a href="index.html" id="mensagem">Voltar para a página inicial</a>
	</div>
</body>
</html>