<!DOCTYPE html>
<html>
<head>
	<title>Inserir inscrição</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="_css/estilo.css">
	<link rel="stylesheet" type="text/css" href="_css/mensagem.css">
</head>
<body>
	<div id="interface">
		<header id="cabecalho">
			<img src="_imagens/logo.png" id="logo">
			<h1>&lsaquo;Kursando cursos&rsaquo;</h1>
			<h2>&lsaquo; Programando para Web &frasl;&rsaquo;</h2>			
		</header>

		<?php
			try{
			$conn = new PDO ('mysql:host=localhost;dbname=kc_cursos','root','');
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			$stmt = $conn->prepare('INSERT INTO contato (nome, telefone, email) VALUES (:nome, :telefone, :email)');
			$nome = $_POST['nome'];
			$telefone = $_POST['telefone'];
			$email = $_POST['email'];
			$stmt->bindValue(':nome', $nome);
			$stmt->bindValue(':telefone', $telefone);
			$stmt->bindValue(':email', $email);
			$stmt->execute();
			echo "Obrigado por escolher a KC cursos, em breve entraremos em contato!";
			}
			catch (PDOException $e){
				echo 'ERROR: '.$e->getMessage();
			}

		?>
		<a href="index.html" id="mensagem">Voltar para a página inicial</a>
	</div>
</body>
</html>